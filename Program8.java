//WAP to find an Armstrong number from an array and return its index.
//input: 10  25  252  36  153  55  89
//output: Armstrong no 153 found at index: 4

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
	
		System.out.println("Enter the size of array:");
		int size=Integer.parseInt(br.readLine());
	
		int arr[]=new int[size];
	
		System.out.println("Enter the Elements :");
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());

		}

		for(int i=0; i<arr.length; i++){

			int temp=arr[i];
			int count=0;
			int sum=0;
			while(temp !=0){

				count++;
				temp=temp/10;
			}
			temp=arr[i];

			while(temp != 0){

				int rem=temp%10;
				int mult=1;
				for(int j=1; j<=count; j++){

					mult=mult*rem;
				}
				sum=sum+mult;
				temp=temp/10;
			}
			if(sum==arr[i]){

				System.out.println("Armstrong no "+arr[i]+" found at index : "+i);
			}
		}
	}
}
