//WAP to reverse each element in an array.
//take size & elements from the user.
//input: 10  25  252  36  564
//output: 01  52  252  63  465

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Size of Array:");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the Elements:");
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());

		}
		System.out.println("Output : ");
		for(int i=0; i<arr.length; i++){

			int rev=0;
			int temp=arr[i];
			while(temp!=0){

				rev=rev*10+temp%10;
				temp=temp/10;
			}
			System.out.println(rev);
		}
	}
}
