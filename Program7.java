// WAP to find Strong number from an array and return its index.
// input: 10  25  252  36  564  145 
// output: Strong no 145 found at index : 5

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array:");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter the Elements: ");
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0; i<arr.length; i++){

			int digit,fact;
			int sum=0;
			int temp=arr[i];
			while(temp!=0){

				fact=1;
				digit=temp%10;
				for(int j=1; j<=digit; j++){

					fact=fact*j;
				}
				temp=temp/10;
				sum=sum+fact;
			}
			if(sum==arr[i]){

				System.out.println("Strong no "+arr[i]+" found at index : "+i);
			}
		}
	}
}
