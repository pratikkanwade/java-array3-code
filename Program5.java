//WAP to find a perfect number from an array and return its index.
//input: 10 25 252 496 564
//output: Prime no 496 found at index : 3

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array:");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter the Elements:");
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());

		}
		
		for(int i=0; i<arr.length; i++){

			int sum=0;
			int temp=arr[i];
			for(int j=1; j<temp; j++){

				if(temp%j==0){
					
					sum=sum+j;		
				}
			}
			if(sum==temp){

				System.out.println("Perfect no "+arr[i]+" found at index : "+i);
			}
		}
	}
}
