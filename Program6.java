//WAP to find a palindrome number from an array and return its index.
//input: 10  25  252  36  564 
//output: Palindrome no 252 found at index : 2

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array:");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter the Elements: ");
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());

		}
		for(int i=0; i<arr.length; i++){

			int temp=arr[i];
			int rev=0;
			while(temp!=0){

				rev=rev*10+temp%10;
				temp=temp/10;
			}
			if(rev==arr[i]){

				System.out.println("Palindrome no "+arr[i]+" found at index : "+i);
			}
		}
	}
}
