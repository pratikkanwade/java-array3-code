//WAP to find a prime number from array and return index .
//input: 10 25 36 566 34  53 50 100
//output: Prime no 53 found at index : 5

import java.io.*;
class Demo{

	public static void main(String[] args)throws IOException{

		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the size of array:");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		System.out.println("Enter the Elements:");
		for(int i=0; i<arr.length; i++){

			arr[i]=Integer.parseInt(br.readLine());
		}

		for(int i=0; i<arr.length; i++){

			int count=0;
			int temp=arr[i];
			for(int j=1; j<=temp; j++){

				if(temp%j==0){

					count++;
				}
			}
			if(count==2){

				System.out.println("Prime no "+arr[i]+" found at index : "+i);
			}
		}
	}
}
